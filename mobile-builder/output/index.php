<?php
header('Access-Control-Allow-Origin: "*"');
header('cache-control: "private, max-age=0, no-cache, no-store"');
header('pragma: "no-cache"');
     
echo '<html>';
echo '<head>';
echo '<meta charset="utf-8">';
echo '<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">';
echo '<title>IMA BuilDeRz | Output</title>';
echo '<link href="../templates/default/css/ionic.min.css" rel="stylesheet" type="text/css" />';
echo '<script src="../templates/default/js/ionic.bundle.min.js"></script>';
echo '<script type="text/javascript">';
echo 'angular.module("starter", ["ionic"]).run(function($ionicPlatform){';
echo '$ionicPlatform.ready(function() {';
echo '});';
echo '})';
echo '</script>';
echo '</head>';
echo '<body ng-app="starter">';
echo '<ion-pane>';

echo '<ion-header-bar class="bar-calm">';
echo '<h1 class="title title title-center">IMA BuilDeRz ~ Outputs</h1>';
echo '</ion-header-bar>';

echo '<ion-content>';
echo '<div class="list card">';
foreach (glob("*/www/index.html") as $filename)
{
    $link = basename(pathinfo(pathinfo($filename, PATHINFO_DIRNAME), PATHINFO_DIRNAME));
    echo '<a class="item item-icon-left assertive" href="' . $link . '/www/" ><i class="icon ion-card"></i>' . $link . '</a>';
    echo '<a class="item item-icon-left assertive" href="' . $link . '/backend/" ><i class="icon ion-card"></i>-- Backend</a>';
}
echo '</div>';
echo '</ion-content>';
echo '</ion-pane>';
echo '</body>';
echo '</html>';

?>