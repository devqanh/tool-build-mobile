<?php

/**
 * @author Jasman <jasman@ihsana.com>
 * @copyright Ihsana IT Solutiom 2016
 * @license Commercial License
 * 
 * @package Ionic App Builder
 */

if (!defined('JSM_EXEC'))
{
    die(':)');
}


if (file_exists('output/ionic.config.json'))
{
    unlink('output/ionic.config.json');
}
if (file_exists('output/ionic.project'))
{
    unlink('output/ionic.project');
}
if (file_exists('output/config.xml'))
{
    unlink('output/config.xml');
}
if (file_exists('output/gui_cordova.ini'))
{
    unlink('output/gui_cordova.ini');
}
unset($_SESSION['PROJECT']['tables']['custom']);


@unlink('projects/' . $_SESSION['FILE_NAME'] . '/page_builder.page_wordpress..json');
@unlink('projects/' . $_SESSION['FILE_NAME'] . '/tables..json');
@unlink('projects/' . $_SESSION['FILE_NAME'] . '/page..json');
@unlink('projects/' . $_SESSION['FILE_NAME'] . '/page._singles.json');

@unlink('output/README.md');
@unlink('output/LICENSE');
@unlink('output/config-phonegap.xml');
@unlink('output/config-ionic.xml');

if (isset($_SESSION['FILE_NAME']))
{
    if (!file_exists('output/' . $_SESSION['FILE_NAME']))
    {
        @mkdir('output/' . $_SESSION['FILE_NAME'], 0777, true);
    }
    
    if(!file_exists('output/' . $_SESSION['FILE_NAME'] . '/www/index.html' )){
        //header('Location: ./?page=dashboard&reset=true');
    }
}

function GuideMarkup($guides)
{
    $page_guide = null;
    if ($_SESSION['GUIDES'] == true)
    {
        if (is_array($guides))
        {
            $guide_lists = '<ul id="guide" data-tourtitle="Open Page Guide for help">';
            foreach ($guides as $guide)
            {
                $guide_lists .= '<li class="tlypageguide_' . $guide['pos'] . '" data-tourtarget="' . $guide['target'] . '">' . $guide['text'] . '</li>';
            }
            $guide_lists .= '</ul>';

            $page_guide = '
                    <div style="display:none">
                        ' . $guide_lists . '            
                        <div class="tlyPageGuideWelcome">
                            <p>Welcome to guides! guide is here to help you learn more.</p>
                            <button class="tlypageguide_start">let\'s go</button>
                            <button class="tlypageguide_ignore">not now</button>
                            <button class="tlypageguide_dismiss">got it, thanks</button>
                        </div>
                    </div>
                    ';
        }
    }
    return $page_guide;
}

function sample_data($type)
{
    $return = null;
    switch ($type)
    {
        case 'text':
            $lipsum = new LoremIpsum();
            $return = $lipsum->words(10);
            break;
        case 'paragraph':
            $lipsum = new LoremIpsum();
            $return = $lipsum->words(20);
            break;
        case 'heading-1':
            $lipsum = new LoremIpsum();
            $return = ucwords($lipsum->words(2));
            break;
        case 'heading-2':
            $lipsum = new LoremIpsum();
            $return = ucwords($lipsum->words(2));
            break;
        case 'heading-3':
            $lipsum = new LoremIpsum();
            $return = ucwords($lipsum->words(2));
            break;
        case 'heading-4':
            $lipsum = new LoremIpsum();
            $return = ucwords($lipsum->words(2));
            break;
        case 'images':
            $return = 'data/images/images/slidebox-' . rand(0, 4) . '.jpg';
            break;
        case 'slidebox':
            $return = 'slide1|slide2';
            break;
        case 'icon':
            $icon = new jsmIonicon();
            $iconList = $icon->iconList();
            $id = rand(0, count($iconList));
            $return = 'ion-' . $iconList[$id]['var'];
            break;
        case 'to_trusted':
            $lipsum = new LoremIpsum();
            $return = $lipsum->sentences(1, 'p');
            $return .= $lipsum->sentences(1, 'blockquote');
            $return .= $lipsum->sentences(5, 'p');
            break;
        case 'link':
            $return = 'http://ihsana.com/?p=' . rand(0, 9);
            break;
        case 'video':
            $return = 'http://www.w3schools.com/html/mov_bbb.mp4';
            break;
        case 'rating':
            $return = rand(2, 5);
            break;
        case 'share_link':
            $return = 'http://goo.gl/D1giIr';
            break;
        case 'ytube':
            $return = '4HkG8z3sa-0';
            break;
        case 'audio':
            $return = 'http://www.w3schools.com/html/horse.mp3';
            break;
        case 'gmap':
            $maps = array(
                '48.85693,2.3412',
                '-6.17149,106.82752',
                '35.68408,139.80885');
            $return = $maps[rand(0, count($maps) - 1)];
            break;
        case 'webview':
            $return = 'http://goo.gl/D1giIr';
            break;
        case 'appbrowser':
            $return = 'http://www.w3schools.com/';
            break;

    }
    return $return;
}
function str2var($string, $strtolower = true,$whitelist='')
{
    $char = 'abcdefghijklmnopqrstuvwxyz_1234567890.[\']:'.$whitelist;

    $Allow = null;
    if ($strtolower == true)
    {
        $string = strtolower($string);
    } else
    {
        $char .= "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    }
    $string = str_replace(array(
        ' ',
        '-',
        '__'), '_', $string);

    $string = str_replace(array('___', '__'), '_', $string);
    for ($i = 0; $i < strlen($string); $i++)
    {
        if (strstr($char, $string[$i]) != false)
        {
            $Allow .= $string[$i];
        }
    }
    return $Allow;
}
 

/**
 * buildIonic()
 * 
 * @param mixed $filename
 * @return void
 */
function buildIonic($filename)
{
    $_SESSION["FILE_LOAD"] = null;
    $_new_config = array();
    $config = array();
    foreach (glob('projects/' . $filename . "/*.json") as $file)
    {
        $_SESSION["FILE_LOAD"]['CONFIG'][] = $file;
        $_config = null;
        $name = pathinfo($file, PATHINFO_FILENAME);
        $_config = json_decode(file_get_contents($file, true), true);
        if (!is_array($_config))
        {
            $_config = array();
        }
        $config = array_merge_recursive($config, $_config);
    }


    $_SESSION["PROJECT"] = $config;
    $ionic = new Ionic('output/', $config);
    $ionic->output();

    foreach (glob('projects/' . $filename . '/img/*.png') as $src_file)
    {
        $new_file = 'output/' . $filename . '/www/img/' . basename($src_file);
        if (copy($src_file, $new_file))
        {
            $_SESSION["FILE_LOAD"]['COPY'][] = $src_file . ' => ' . $new_file;
        }

    }

    $source_css = 'resources/lib/ionic/css/ionic.min.css';
    if (file_exists($source_css))
    {
        $new_css = 'output/' . $filename . '/www/lib/ionic/css/ionic.min.css';
        @copy($source_css, $new_css);
    }
    $source_css = 'resources/lib/ionic-material/css/ionic.material.min.css';
    if (file_exists($source_css))
    {
        $new_css = 'output/' . $filename . '/www/lib/ionic-material/ionic.material.min.css';
        @copy($source_css, $new_css);
    }

    foreach (glob('projects/' . $filename . '/tables/*.json') as $src_file)
    {
        $new_file = 'output/' . $filename . '/www/data/tables/' . basename($src_file);
        if (copy($src_file, $new_file))
        {
            $_SESSION["FILE_LOAD"]['COPY'][] = $src_file . ' => ' . $new_file;
        }
    }

}

function notice()
{
    $notice = null;
    $bs = new jsmBootstrap();
    if (!isset($_GET['notice']))
    {
        $_GET['notice'] = null;
    }
    if (!isset($_GET['err']))
    {
        $_GET['err'] = 'null';
    }
    if ($_GET['err'] == 'null')
    {
        switch ($_GET['notice'])
        {
            case 'save':
                $notice = $bs->Alerts(null, 'Item has been successfully save!', 'success', true);
                break;
            case 'delete':
                $notice = $bs->Alerts(null, 'Item has been successfully delete! ', 'success', true);
                break;
            case 'create':
                $notice = $bs->Alerts(null, 'Item has been successfully created! ', 'success', true);
                break;
        }
    }

    if ($_GET['err'] == 'project')
    {
        $notice = $bs->Modal('error-modal', 'Ops! Project', '<p>Please select project or create new project for first time.</p>', 'sm', null, 'Close', false);
    }

    if (!isset($_SESSION['PAGE_ERROR']))
    {
        $_SESSION['PAGE_ERROR'] = array();
    }
    if (count($_SESSION['PAGE_ERROR']) != 0)
    {
        $msg = '<p>Some pages can not be rewritten, it is because the page is locked, <br/>go to <code>(IMAB) Pages</code> -&raquo; <code>Page Manager</code> --&raquo; click key icon for <code>lock/unlock</code>.</p>';
        $msg .= '<ul>';
        foreach ($_SESSION['PAGE_ERROR'] as $page_error)
        {
            $msg .= '<li>' . $page_error . '</li>';
        }
        $msg .= '</ul>';
        $notice = $bs->Modal('error-modal', 'Ops! Some pages is locked', $msg, 'md', null, 'Close', false);
    }
    $_SESSION['PAGE_ERROR'] = array();

    if ($_GET['err'] == 'true')
    {
        switch ($_GET['notice'])
        {
            case 'format':
                $notice = $bs->Alerts(null, 'Invalid format!', 'danger', true);
                break;
            case 'exist':
                $notice = $bs->Alerts(null, 'Data already exist, please delete first!', 'danger', true);
                break;
        }
    }


    // TODO: CHECK INDEX
    if (isset($_SESSION['PROJECT']['app']))
    {
        if ($_GET['page'] != 'x-import-project')
        {
            if ($_GET['page'] != 'dashboard')
            {
                if ($_GET['page'] != 'menu')
                {
                    $msg_notice = 'Please select page as homepage or index!!!<br/>Go to <strong>page</strong> menu then select one page <strong>index</strong>.';
                    if (!isset($_SESSION['PROJECT']['app']['index']))
                    {
                        $notice = $bs->Modal('error-modal', 'Ops! Home page is error', $msg_notice, 'md', null, 'Close', false);
                    } else
                    {
                        if (!isset($_SESSION['PROJECT']['page']))
                        {
                            $_SESSION['PROJECT']['page'] = array();
                        }
                        $index_prefix = $_SESSION['PROJECT']['app']['index'];
                        $x_pages = $_SESSION['PROJECT']['page'];
                        $page_index_error = true;
                        foreach ($x_pages as $x_page)
                        {
                            if ($x_page['prefix'] == $index_prefix)
                            {
                                $page_index_error = false;
                            }
                        }

                        if ($page_index_error == true)
                        {
                            $notice = $bs->Modal('error-modal', 'Ops! Home page is error', $msg_notice, 'md', null, 'Close', false);
                        }
                    }
                }
            }
        }
    }
    return $notice;
}

function googleplay_link()
{
    return "market://details?id=" . JSM_PACKAGE_NAME . "." . str_replace('_', '', str2var($_SESSION['PROJECT']['app']['company'])) . '.' . str_replace('_', '', str2var($_SESSION['PROJECT']['app']['prefix']));
}
function mailto_link()
{
    return "mailto:" . $_SESSION['PROJECT']['app']['author_email'];
}

?>