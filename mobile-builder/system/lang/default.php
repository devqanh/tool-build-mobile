<?php

/**
 * @author Jasman
 * @copyright 2017
 */

$lang['id']['Name'] = 'Nama';
$lang['id']['Unicode Name'] = 'Nama Dalam Unicode';
$lang['id']['Description'] = 'Deskripsi';
$lang['id']['Enter your aplication name, only the characters a-z and spaces allowed.'] = 'Masukan nama aplikasi kamu, hanya boleh karakter a-z dan spasi';
$lang['id']['a brief description of your application.'] = 'Deskripsi singkat aplikasi kamu.';
$lang['id']['Used for language localization, keep blank for default'] = 'Digunakan untuk bahasa daerah, biarkan kosong untuk biasanya';
?>