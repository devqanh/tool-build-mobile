<?php

/**
 * @author Jasman <jasman@ihsana.com>
 * @copyright Ihsana IT Solution 2011
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * @package String Class
 * 
 * 
 * $String = new String;
 * echo $String->Filter($content, "number");
 * 
 * echo $string->Convert($string, $parameter, $WhiteList);
 * 
 * Parameter:
 * - alphabet
 * - username
 * - email
 * - number
 * - url
 * - paragraph
 * 
 * WhiteList:
 * - null;
 * 
 * Return:
 * - false
 * - string
 * 
 */
defined('JSM_EXEC') or die('Not Here');
class jsmString
{
    protected $Alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    protected $Number = '0123456789';
    protected $Username = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-.';
    protected $Filesystem = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-[]./\\';
    protected $Email = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-.@';
    protected $Url = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-[]./\\:';
    protected $Paragraph = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789|+_) (*&^%$#@!_+|?><,./;}{][';
    public function Convert($string, $type = "alphabet", $WhiteList = null)
    {
        $this->Type = strtolower(trim($type));
        switch ($this->Type)
        {
            case 'email':
                $this->String = strtolower($string);
                break;
            case 'username':
                $this->String = strtolower($string);
                break;
            case 'id':
                $this->String = str_replace(' ', '_', strtolower($string));
                break;
            case 'number':
                $this->String = $string;
                break;
            case 'paragraph':
                $this->String = $string;
                break;
            case 'alphabet':
                $this->String = $string;
                break;
            case 'url':
                $this->String = str_replace(' ', '_', strtolower($string));
                break;
        }
        $this->WhiteList = $WhiteList;
        return $this->FilterString();
    }
    public function Filter($string, $type = "email")
    {
        $this->Type = strtolower(trim($type));
        $is_valid = false;
        switch ($this->Type)
        {
            case "ip":
                $is_valid = $this->FilterIp($string);
                break;
            case "email":
                $is_valid = $this->FilterEmail($string);
                break;
            case "number":
                $is_valid = $this->FilterNumber($string);
                break;
            case "url":
                $is_valid = $this->FilterUrl($string);
                break;
        }
        return $is_valid;
    }
    protected function FilterUrl($str)
    {
        $str = filter_var($str, FILTER_VALIDATE_URL);
        return $str;
    }
    protected function FilterEmail($str)
    {
        $str = filter_var($str, FILTER_VALIDATE_EMAIL);
        return $str;
    }
    protected function FilterIp($str)
    {
        $str = filter_var($str, FILTER_VALIDATE_IP);
        return $str;
    }
    protected function FilterNumber($str)
    {
        if (is_numeric($str))
        {
            return $str;
        } else
        {
            return false;
        }
    }
    protected function Character()
    {
        $Charset = null;
        switch ($this->Type)
        {
            case "alphabet":
                $Charset = $this->Alphabet;
                break;
            case "id":
                $Charset = strtolower($this->Username);
                break;
            case "username":
                $Charset = strtolower($this->Username);
                break;
            case "email":
                $Charset = strtolower($this->Email);
                break;
            case "number":
                $Charset = $this->Number;
                break;
            case "url":
                $Charset = $this->Url;
                break;
            case "paragraph":
                $Charset = $this->Paragraph;
                break;
        }
        if ($this->WhiteList != null)
        {
            $Charset .= $this->WhiteList;
        }
        return $Charset;
    }
    protected function FilterString()
    {
        $Allow = null;
        $char = $this->Character();
        $string = $this->String;
        for ($i = 0; $i < strlen($string); $i++)
        {
            $valid = false;
            for ($x = 0; $x < strlen($char); $x++)
            {
                if (ord($string[$i]) == ord($char[$x]))
                {
                    $valid = true;
                }
            }
            if ($valid == true)
            {
                $Allow .= $string[$i];
            }
        }
        return $Allow;
    }
    function Check($string, $type = "username", $WhiteList = null)
    {
        $text = $this->Convert($string, $type, $WhiteList);
        //echo md5($string) .":".md5($text);
        if (md5($string) == md5($text))
        {
            return false;
        } else
        {
            return true;
        }
    }
}

?>