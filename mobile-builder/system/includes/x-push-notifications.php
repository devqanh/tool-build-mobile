<?php

/**
 * @author Jasman <jasman@ihsana.com>
 * @copyright Ihsana IT Solutiom 2016
 * @license Commercial License
 * 
 * @package Ionic App Builder
 */

if (!defined('JSM_EXEC'))
{
    die(':)');
}
$file_name = 'test';
$bs = new jsmBootstrap();
$push_content = $html = $content = null;
if (isset($_SESSION['FILE_NAME']))
{
    $file_name = $_SESSION['FILE_NAME'];
} else
{
    header('Location: ./?page=dashboard&err=project');
    die();
}
if (!isset($_SESSION["PROJECT"]['menu']))
{
    header('Location: ./?page=menu&err=new');
    die();
}

$out_path = 'output/' . $file_name;
if (!isset($_GET['prefix']))
{
    $_GET['prefix'] = '';
}

$push_path = 'projects/' . $file_name . '/push.json';

if (isset($_POST['push-save']))
{
    $push_code = $_POST['push'];
    file_put_contents($push_path, json_encode(array('push' => $push_code)));
    $mod_push = 'projects/' . $file_name . '/mod.notification.json';
    if ($push_code['plugin'] == 'none')
    {
        @unlink($mod_push);
    } else
    {
        $new_mod['mod']['notification']['name'] = $push_code['plugin'];
        $new_mod['mod']['notification']['engines'] = 'cordova';
        file_put_contents($mod_push, json_encode($new_mod));
    }
    buildIonic($file_name);
    header('Location: ./?page=x-push-notifications&err=null&notice=save');
    die();
}

$raw_push['push']['plugin'] = 'none';
$raw_push['push']['app_id'] = '';
$raw_push['push']['app_key'] = '';

if (file_exists($push_path))
{
    $raw_push = json_decode(file_get_contents($push_path), true);
}


$push_content = null;
$cordova_plugin[] = array('label' => 'none', 'value' => 'none');
$cordova_plugin[] = array('label' => 'onesignal-cordova-plugin (recommended)', 'value' => 'onesignal-cordova-plugin');
$cordova_plugin[] = array('label' => 'cordova-plugin-fcm (deprecated)', 'value' => 'cordova-plugin-fcm');
//$cordova_plugin[] = array('label' => 'phonegap-plugin-push', 'value' => 'phonegap-plugin-push');


$z = 0;
foreach ($cordova_plugin as $_cordova_plugin)
{
    $cordova_plugins[$z] = $_cordova_plugin;
    if ($raw_push['push']['plugin'] == $_cordova_plugin['value'])
    {
        $cordova_plugins[$z]['active'] = true;
    }
    $z++;
}
if(!isset($raw_push['push']['app_key'])){
    $raw_push['push']['app_key'] ='';
}
$push_content .= $bs->FormGroup('push[plugin]', 'default', 'select', 'Using Cordova Plugin', $cordova_plugins, '<code>cordova-plugin-fcm</code> not support for Adobe Phonegap Build (Online)', '', '8');
$push_content .= $bs->FormGroup('push[app_id]', 'default', 'text', 'OneSignal AppID', 'c6c7cc44-75d3-4c0b-8a4b-d3a2e0432ece', 'Your OneSignal AppId, available in <a href="https://documentation.onesignal.com/docs/accounts-and-keys#section-keys-ids">OneSignal</a>', '', '8', htmlentities($raw_push['push']['app_id']));
$push_content .= $bs->FormGroup('push[app_key]', 'default', 'text', 'OneSignal AppKey (Required for Web-Admin Generator)', 'ZThaNjNvOTctY2RjYi00ZjUxLTgxMTItNDg2NTRkNmY3MGVk', 'Your OneSignal AppKey', '', '8', htmlentities($raw_push['push']['app_key']));

$list_page = array();
foreach ($_SESSION['PROJECT']['page'] as $_page)
{
    $list_page[] = $_page['prefix'];
}

if ($raw_push['push']['plugin'] != 'none')
{

    switch ($raw_push['push']['plugin'])
    {
        case 'onesignal-cordova-plugin':
            $push_content .= '<blockquote class="blockquote blockquote-danger"><h4>The rules that apply are:</h4><ul>';
            $push_content .= '<li>You need install <code>' . $raw_push['push']['plugin'] . '</code> or follow IMA BuildeRz Guides (in Dashboard -> How to build?):</p>';
            $push_content .= '<pre class="shell">cordova plugin add ' . $raw_push['push']['plugin'] . ' --save</pre></li>';
            $push_content .= '<li>Onesignal additional data variable <code>page</code> used for open specific pages, example value: <code>'.implode('</code>, <code>',$list_page).'</code>, or page with param example: <code>page_prefix/1</code></li>';
            $push_content .= '<li>official docs oneSignal: <a target="_blank" href="https://documentation.onesignal.com/docs/ionic-sdk-setup">https://documentation.onesignal.com/docs/ionic-sdk-setup</a></li>';
            $push_content .= '</ul></blockquote>';
            break;
        case 'cordova-plugin-fcm':
            $push_content .= '<blockquote class="blockquote blockquote-danger">Not recommended to be used in conjunction with <code>cordova-plugin-admobpro</code> and using <code>Adobe Phonegap Online</code></blockquote>';
            $push_content .= '<p>You need install <code>' . $raw_push['push']['plugin'] . '</code> or follow IMA BuildeRz Guides (in Dashboard -> How to build?):</p>';
            $push_content .= '<pre class="shell">cordova plugin add ' . $raw_push['push']['plugin'] . ' --save</pre>';

            $push_content .= '
<p>Usage</p>
<ol>
<li>Install plugin <strong><em>cordova plugin add cordova-plugin-fcm</em></strong></li>
<li>Create project on <a href="https://console.firebase.google.com">https://console.firebase.google.com</a></li>
<li>Add new apps with Aplication ID: <kbd>' . JSM_PACKAGE_NAME . '.' . str_replace('_', '', str2var($_SESSION["PROJECT"]["app"]["company"])) . '.' . str_replace('_', '', $_SESSION["PROJECT"]["app"]["prefix"]) . '</kbd></li>
<li>For android platform, copy <strong>google-service.json</strong> file to root (<strong class="text-danger">your_app/google-service.json</strong>) for latest version or <strong>your_app/platforms/android/google-service.json</strong> for version 1.x</li>
<li><em>ionic build android</em> for build apk</li>
<li>Go to your project in <a href="https://console.firebase.google.com">https://console.firebase.google.com</a> and select <strong>Notifications</strong> from menu -&gt; *<em>New message</em> -&gt; select your app, add message and click send</li>
</ol>

<p>Our Reference:</p>
<ul>
<li><a target="_blank" href="https://www.npmjs.com/package/cordova-plugin-fcm">Google Firebase Cloud Messaging Cordova Push Plugin</a></li>
<li><a target="_blank" href="https://github.com/edismooth/ionic2-firebase">ionic2-firebase</a></li>
<li><a target="_blank" href="https://firebase.google.com/docs/cloud-messaging/ios/certs">Provisioning APNs SSL Certificates</a></li>
</ul>
';

            break;
        case 'phonegap-plugin-push':
            $push_content .= '<p>You need install cordova plugin:</p>';
            $push_content .= '<pre class="shell">cordova plugin add ' . $raw_push['push']['plugin'] . ' --save</pre>';
            break;
    }

}

$button[] = array(
    'name' => 'push-save',
    'label' => 'Save Push Notifications',
    'tag' => 'submit',
    'color' => 'primary');


$push_content .= $bs->FormGroup(null, 'default', 'html', null, $bs->ButtonGroups(null, $button));


$content = null;
$content .= '<h4><span class="fa-stack fa-lg"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-server fa-stack-1x"></i></span>Extra Menus -&raquo; (IMAB) Push Notifications</h4>';
$content .= '<blockquote class="blockquote blockquote-danger"><p>Push Notifications only work in real device, it\'s will <ins>not be displayed on the (IMAB) Emulator</ins>.</p></blockquote>';

$content .= '<div class="row">';
$content .= '<div class="col-md-8">';
$content .= '<div class="panel panel-default">';
$content .= '<div class="panel-heading"><h4 class="panel-title">General</h4></div>';
$content .= '<div class="panel-body">';
$content .= notice();
$content .= $bs->Forms('app-setup', '', 'post', 'default', $push_content);
$content .= '</div>';
$content .= '</div>';
$content .= '</div>';
$content .= '<div class="col-md-4">';
$content .= '<div class="panel panel-default">';
$content .= '<div class="panel-heading"><h4 class="panel-title">Information</h4></div>';
$content .= '<div class="panel-body">';

$content .= '<dl>';
$content .= '<dt>Project/App Name</dt><dd>' . $_SESSION['PROJECT']['app']['name'] . '</dd>';
$content .= '<dt>Package Name</dt><dd><code>' . JSM_PACKAGE_NAME . '.' . str_replace('_', '', str2var($_SESSION["PROJECT"]["app"]["company"])) . '.' . str_replace('_', '', $_SESSION["PROJECT"]["app"]["prefix"]) . '</code></dd>';
$content .= '</dl>';

$content .= '</div>';
$content .= '</div>';
$content .= '</div>';
$content .= '</div>';
 


$template->demo_url = $out_path . '/www/#/';
$template->title = $template->base_title . ' | ' . 'Extra Menus -&raquo; Push Notifications';
$template->base_desc = '';
$template->content = $content;
$template->emulator = false;
?>