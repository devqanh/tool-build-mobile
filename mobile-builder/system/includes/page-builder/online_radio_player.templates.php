<?php

if (isset($_POST['page-builder']))
{
    if ($_GET['target'] != '')
    {
        if (isset($_POST['page_target']))
        {
            $_GET['target'] = $_POST['page_target'];
        }

        $postdata['prefix'] = str2var($_GET['target']);
        $postdata['radio_name'] = $_POST['radio_name'];
        $postdata['radio_url'] = $_POST['radio_url'];

        $json_save['page_builder']['online_radio_player'][$postdata['prefix']] = $postdata;
        file_put_contents('projects/' . $_SESSION['FILE_NAME'] . '/page_builder.online_radio_player.' . $postdata['prefix'] . '.json', json_encode($json_save));

        $new_page_content = '
<div class="audioplayer-box">

	<div class="audioplayer-card text-center">
    
        <div ng-if="toggle_state" class="audioplayer-logo">
            <ion-spinner ng-if="state==0" class="audioplayer-spinner" icon="dots"></ion-spinner>
            <ion-spinner ng-if="state==1" class="audioplayer-spinner" icon="ios-small"></ion-spinner>
            <ion-spinner ng-if="state==2" class="audioplayer-spinner" icon="ios"></ion-spinner>
            <ion-spinner ng-if="state==3" class="audioplayer-spinner" icon="ios"></ion-spinner>
            <ion-spinner ng-if="state==4" class="audioplayer-spinner" icon="ripple"></ion-spinner>
        </div>
      
        <div ng-if="!toggle_state" class="audioplayer-logo">
            <div class="audioplayer-spinner">
                <i class="ion-volume-mute"></i>
            </div>
        </div>
      
        <div class="audioplayer-title">
        	<h3>' . $postdata['radio_name'] . '</h3>       
        </div>
    		
        <div class="text-center">
    		<button class="button button-fab-large button-fab  button-assertive" ng-click="togglePlay()">
              <i ng-if="toggle_state" class="icon ion-play"></i>
              <i ng-if="!toggle_state" class="icon ion-pause"></i>
    		</button>
    	</div>
        
	</div>
</div>
';

        $new_page_js = '
$scope.radioURL = "' . $postdata['radio_url'] . '";
$scope.toggle_state = false;

if (audioPlayer == null) {
	var audioPlayer = $document[0].createElement("audio");
	audioPlayer.src = $sce.trustAsResourceUrl($scope.radioURL);
	try {
		audioPlayer.play();
		$scope.toggle_state = true;
	} catch (e) {
		$scope.toggle_state = false;
        console.log(e);
	}
}

$scope.togglePlay = function() {
	if ($scope.toggle_state == false) {
		try {
			audioPlayer.play();
			$scope.toggle_state = true;
		} catch (e) {
			$scope.toggle_state = false;
            console.log(e);
		}

	} else {
		$scope.toggle_state = false;
		audioPlayer.pause();
	}
}


$interval(function() {
	$scope.state = audioPlayer.readyState;
}, 500);

';
        $new_page_prefix = $postdata['prefix'];
        $new_page_class = '';
        $new_page_title = htmlentities($postdata['radio_name']);
        $new_page_css = '
.audioplayer-box{min-height: 100%;height: auto; overflow: hidden;position: relative;}
.audioplayer-card{padding:10px;background-color: transparent; margin-top: 40px;}
.audioplayer-title{text-align:center;}
.audioplayer-title h3{color:#fff;}
.audioplayer-button{padding-top: 2px; font-size:42px !important; text-shadow: 0 0 1px #000;color: #ffffff !important;box-shadow: unset !important;} 
.audioplayer-spinner svg {width:128px;height:128px;stroke:#0df;fill:#fff;}
.audioplayer-spinner i{color:#0df;font-size:128px;opacity: 0.9;}   
.audioplayer-logo {height: 130px;width: 100%;display: block;margin-top:12px;margin-bottom:12px;}     
    ';

        create_page($new_page_class, $new_page_title, $new_page_prefix, $new_page_content, $new_page_css, $new_page_js, true, 'ion-play', false, false, false);
    }
}



$project = new ImaProject();
$option_page[] = array('label' => '< page >', 'value' => '');
$z = 1;
foreach ($project->get_pages() as $page)
{
    $option_page[$z] = array('label' => 'Page `' . $page['title'] . '`', 'value' => $page['prefix']);
    if ($_GET['target'] == $page['prefix'])
    {
        $option_page[$z]['active'] = true;
    }
    $z++;
}

$pagebuilder_file = 'projects/' . $_SESSION['FILE_NAME'] . '/page_builder.online_radio_player.' . str2var($_GET['target']) . '.json';
$raw_data = array();
if (file_exists($pagebuilder_file))
{
    $get_raw_data = json_decode(file_get_contents($pagebuilder_file), true);
    $raw_data = $get_raw_data['page_builder']['online_radio_player'][str2var($_GET['target'])];
}



if (!isset($raw_data['radio_name']))
{
    $raw_data['radio_name'] = 'IMA Radio Online';
}

if (!isset($raw_data['radio_url']))
{
    $raw_data['radio_url'] = 'http://192.168.0.1:8600/;?.mp3';
}

$form_input .= $bs->FormGroup('page_target', 'horizontal', 'select', 'Page Target', $option_page, 'Page will be overwritten', null, '4');

if ($_GET['target'] != '')
{
    $form_input .= $bs->FormGroup('radio_name', 'horizontal', 'text', 'Radio Name', 'Radio Name', 'Radio Name', '', '6', $raw_data['radio_name']);
    $form_input .= $bs->FormGroup('radio_url', 'horizontal', 'text', 'Radio URL', 'http://192.168.0.1:8600/;?.mp3', 'example: http://192.168.0.1:8600/;?.mp3', '', '5', $raw_data['radio_url']);
}
$footer .= '
<script type="text/javascript">
     $("#page_target").on("change",function(){
        window.location= "./?page=x-page-builder&prefix=online-radio-player&target=" +  $("#page_target").val() ;
        return false;
     });
</script>
';

?>