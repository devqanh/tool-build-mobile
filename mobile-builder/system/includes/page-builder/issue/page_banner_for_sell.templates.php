<?php

/**
 * @author Jasman <jasman@ihsana.com>
 * @copyright Ihsana IT Solutiom 2016
 * @license Commercial License
 * 
 * @package Ionic App Builder
 */

if (isset($_SESSION['FILE_NAME']))
{
    $file_name = $_SESSION['FILE_NAME'];
} else
{
    header('Location: ./?page=dashboard&err=project');
    die();
}

$require_target_page = true;

$table_banner_for_sell = json_decode(file_get_contents(JSM_PATH . '/system/includes/page-builder/page_banner_for_sell/json/table.banner_for_sell.json'), true);

//$page_posts = json_decode(file_get_contents(JSM_PATH . '/system/includes/page-builder/page_banner_for_sell/json/page.posts.json'), true);
//$page_post_singles = json_decode(file_get_contents(JSM_PATH . '/system/includes/page-builder/page_banner_for_sell/json/page.post_singles.json'), true);

$background = 'data/images/background/bg3.jpg';
$per_page = 12;
if (isset($_POST['page-builder']))
{
    if (isset($_POST['page_target']))
    {
        $postdata['prefix'] = $_POST['page_target'];
    }

    $var = $postdata['prefix'];

    // TODO: page builder settings
    $json_save['page_builder']['page_banner_for_sell'][$var]['wp_url'] = htmlentities($_POST['page_banner_for_sell']['wp_url']);
    $json_save['page_builder']['page_banner_for_sell'][$var]['notes'] = $_POST['page_banner_for_sell']['notes'];

    $site = $json_save['page_builder']['page_banner_for_sell'][$var]['wp_url'];
    $notes = $json_save['page_builder']['page_banner_for_sell'][$var]['notes'];
    file_put_contents('projects/' . $_SESSION['FILE_NAME'] . '/page_builder.page_banner_for_sell.' . $var . '.json', json_encode($json_save));

    // TODO: create table
    unset($table_banner_for_sell['tables']['post']['db_url_dinamic']);
    $_table_banner_for_sell['tables'][$var] = $table_banner_for_sell['tables']['banner_for_sell'];
    $_table_banner_for_sell['tables'][$var]['db_url'] = '';
    $_table_banner_for_sell['tables'][$var]['prefix'] = $var;
    $_table_banner_for_sell['tables'][$var]['parent'] = 'none';
    $_table_banner_for_sell['tables'][$var]['title'] = $var;
    $_table_banner_for_sell['tables'][$var]['bookmarks'] = 'none';
    file_put_contents(JSM_PATH . '/projects/' . $file_name . '/tables.' . $var . '.json', json_encode($_table_banner_for_sell));

    // TODO: + page -+- posts

    $page_posts['page'][0]['prefix'] = 'form_' . $var;
    $page_posts['page'][0]['img_bg'] = $background;
    $page_posts['page'][0]['parent'] = $_SESSION['PROJECT']['menu']['type'];
    //$page_posts['page'][0]['lock'] = true;
    $page_posts['page'][0]['menutype'] = $_SESSION['PROJECT']['menu']['type'];
    $page_posts['page'][0]['title'] = 'Form Order';
    $page_posts['page'][0]['query_value'] = '';
    $page_posts['page'][0]['menu'] = $file_name;
    $page_posts['page'][0]['js'] = '';
    $page_posts['page'][0]['for'] = 'page_builder';
    $page_posts['page'][0]['content'] = '
<div class="list card"  >
	<form ng-submit="submit' . ucwords($var) . '()">
    
    <div class="item item-divider">
        Ad Information
    </div>
        
	<label class="item item-input item-stacked-label">
		<span class="input-label">ad Title</span>
		<input type="text" ng-model="form_' . $var . '.adtitle" name="adtitle" placeholder="adtitle" />
	</label>
	
    <div class="item item-divider">
        Ad Type
    </div>
    
    <ion-radio ng-value="\'Plain Text\'"  ng-model="form_' . $var . '.adtype" name="adtype">
		Plain Text
	</ion-radio>
    
    <ion-radio ng-value="\'Rich Content\'"  ng-model="form_' . $var . '.adtype" name="adtype">
		Rich Content
	</ion-radio>
    
    <ion-radio ng-value="\'Image Ad\'"  ng-model="form_' . $var . '.adtype" name="adtype">
		Image Ad
	</ion-radio>
    	
    <div class="item item-divider">
        Ad Placements
    </div>
        
    <ion-radio ng-value="\'Header\'"  ng-model="form_' . $var . '.adlayout" name="adlayout">
		Header
	</ion-radio>
    
    <ion-radio ng-value="\'Footer\'"  ng-model="form_' . $var . '.adlayout" name="adlayout">
		Footer 
	</ion-radio>
        
    <ion-radio ng-value="\'Before Content\'"  ng-model="form_' . $var . '.adlayout" name="adlayout">
		Before Content 
	</ion-radio>
    
    <ion-radio ng-value="\'After Content\'"  ng-model="form_' . $var . '.adlayout" name="adlayout">
		After Content
	</ion-radio>
    
    <ion-radio ng-value="\'Content\'"  ng-model="form_' . $var . '.adlayout" name="adlayout">
		Content
	</ion-radio>
                
 
    
	<div class="item item-button noborder">
		<button class="button button-assertive ink">Submit</button>
	</div>
    
	</form>
</div>
<br/><br/><br/><br/>
    ';
    file_put_contents(JSM_PATH . '/projects/' . $file_name . '/page.form_' . $var . '.json', json_encode($page_posts));

    $page_posts['page'][0]['prefix'] = $var;
    $page_posts['page'][0]['img_bg'] = $background;
    $page_posts['page'][0]['parent'] = $_SESSION['PROJECT']['menu']['type'];
    //$page_posts['page'][0]['lock'] = true;
    $page_posts['page'][0]['menutype'] = $_SESSION['PROJECT']['menu']['type'];
    $page_posts['page'][0]['title'] = 'Banner for Sell';
    $page_posts['page'][0]['query_value'] = '';
    $page_posts['page'][0]['for'] = 'page_builder';
    $page_posts['page'][0]['menu'] = $file_name;
    $page_posts['page'][0]['js'] = '';
    $page_posts['page'][0]['content'] = '
    
<div class="card">
    <div class="item item-body">
        <div>' . $notes . '</div>
        <button class="button button-full button-positive" ui-sref="' . $file_name . '.form_' . $var . '">
            Order Barner
        </button>
    </div>
</div>
    ';
    file_put_contents(JSM_PATH . '/projects/' . $file_name . '/page.' . $var . '.json', json_encode($page_posts));

    buildIonic($file_name);
    header('Location: ./?page=x-page-builder&prefix=page_banner_for_sell&target=' . $var);
    die();

}
$var = str2var($_GET['target']);
$pagebuilder_file = 'projects/' . $_SESSION['FILE_NAME'] . '/page_builder.page_banner_for_sell.' . $var . '.json';


$raw_data = array();
if (file_exists($pagebuilder_file))
{
    $_raw_data = json_decode(file_get_contents($pagebuilder_file), true);
    $raw_data = $_raw_data['page_builder']['page_banner_for_sell'][$var];
}
if (!isset($raw_data['wp_url']))
{
    $raw_data['wp_url'] = 'http://your_wordpress.org/';
}

if (!isset($raw_data['notes']))
{
    $raw_data['notes'] = '<h4>Vivamus porta ante</h4><p>Etiam sit amet ex ullamcorper ante mattis sodales. Duis dapibus, quam ac elementum suscipit, felis leo eleifend tortor.</p>';
}
// TODO: page target
$project = new ImaProject();
$out_path = 'output/' . $file_name;
$preview_url = $out_path . '/www/#/' . $_SESSION['PROJECT']['app']['prefix'] . '/' . $var;


$option_page[] = array('label' => '< select page >', 'value' => '');
$z = 1;
foreach ($project->get_pages() as $page)
{
    $option_page[$z] = array('label' => 'Page `' . $page['prefix'] . '`', 'value' => $page['prefix']);
    if ($_GET['target'] == $page['prefix'])
    {
        $option_page[$z]['active'] = true;
    }
    $z++;
}


$json_categories = $raw_data['wp_url'] . '/wp-json/wp/v2/categories';

$form_input .= '<blockquote class="blockquote blockquote-warning">';
$form_input .= '<p>Your WordPress Plugin requires REST API v2 and REST-API Helper, then must be active in your WordPress Site.</p>';
$form_input .= '<ol>';
$form_input .= '<li>Download <a href="https://wordpress.org/plugins/rest-api/">WordPress REST API 2</a> and <a href="https://wordpress.org/plugins/rest-api-helper/">REST API Helper</a></li>';
$form_input .= '<li>Unzip and Upload `rest-api.xxx.zip` to the `/wp-content/plugins/rest-api` directory</li>';
$form_input .= '<li>Activate the plugin through the \'plugins\' menu in WordPress</li>';
$form_input .= '<li>Followed by unzip and Upload `rest-api-helper.xxx.zip` to the `/wp-content/plugins/rest-api-helper` directory</li>';
$form_input .= '<li>Then activate the plugin through the \'plugins\' menu </li>';
$form_input .= '<li>Now save and please fill in the fields below:</li>';
$form_input .= '</ol>';
$form_input .= '</blockquote>';
$form_input .= '<hr/>';

$form_input .= $bs->FormGroup('page_target', 'horizontal', 'select', 'Page Target', $option_page, 'Page will be overwritten', null, '4');

$form_input .= '<h4>Settings</h4>';
$form_input .= $bs->FormGroup('page_banner_for_sell[wp_url]', 'horizontal', 'text', 'WordPress URL', 'http://demo.ihsana.net/wordpress/', '', null, '7', $raw_data['wp_url']);
$form_input .= $bs->FormGroup('page_banner_for_sell[notes]', 'horizontal', 'textarea', 'Notes', '21', '', '', '7', $raw_data['notes']);


$footer .= '
<script src="./templates/default/vendor/tinymce/tinymce.min.js"></script>
<script type="text/javascript">

    tinymce.init({
        selector : "#page_banner_for_sell_notes_",
        force_br_newlines : false,
        force_p_newlines : false,
        forced_root_block : "",
        
    });

     $("#page_target").on("change",function(){
        window.location= "./?page=x-page-builder&prefix=page_banner_for_sell&target=" +  $("#page_target").val() ;
        return false;
     });
</script>
';

?>