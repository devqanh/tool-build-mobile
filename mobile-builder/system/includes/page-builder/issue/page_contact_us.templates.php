<?php

/**
 * @author Jasman <jasman@ihsana.com>
 * @copyright Ihsana IT Solutiom 2016
 * @license Commercial License
 * 
 * @package Ionic App Builder
 */

if (isset($_SESSION['FILE_NAME']))
{
    $file_name = $_SESSION['FILE_NAME'];
} else
{
    header('Location: ./?page=dashboard&err=project');
    die();
}

$require_target_page = true;
if (isset($_POST['page-builder']))
{
    if (isset($_POST['page_target']))
    {
        $postdata['prefix'] = $_POST['page_target'];
    }

    $var = $postdata['prefix'];
    // TODO: page builder settings
    $json_save['page_builder']['contact_us'][$var]['wp_url'] = htmlentities($_POST['contact_us']['wp_url']);
    $json_save['page_builder']['contact_us'][$var]['notes'] = $_POST['contact_us']['notes'];

    $site = $json_save['page_builder']['contact_us'][$var]['wp_url'];
    $notes = $json_save['page_builder']['contact_us'][$var]['notes'];
    file_put_contents('projects/' . $_SESSION['FILE_NAME'] . '/page_builder.contact_us.' . $var . '.json', json_encode($json_save));

    // TODO: intro page
    $_page['page'][0]['builder_link'] = @$_SERVER["HTTP_REFERER"];
    $_page['page'][0]['prefix'] = $var;
    $_page['page'][0]['img_bg'] = $background;
    $_page['page'][0]['parent'] = $_SESSION['PROJECT']['menu']['type'];
    $_page['page'][0]['lock'] = true;
    $_page['page'][0]['menutype'] = $_SESSION['PROJECT']['menu']['type'];
    $_page['page'][0]['title'] = 'Contact Us';
    $_page['page'][0]['query_value'] = '';
    $_page['page'][0]['for'] = 'page_builder';
    $_page['page'][0]['menu'] = $file_name;
    $_page['page'][0]['js'] = '$ionicConfig.backButton.text("");';
    $_page['page'][0]['content'] = '
    
<div class="card">
    <div class="item item-body">
        <div>' . $notes . '</div>
        <button class="button button-full button-positive" ui-sref="' . $file_name . '.form_' . $var . '">
            Contact Us
        </button>
    </div>
</div>
    ';
    file_put_contents(JSM_PATH . '/projects/' . $file_name . '/page.' . $var . '.json', json_encode($_page));


    // TODO: intro page
    $_page['page'][0]['builder_link'] = @$_SERVER["HTTP_REFERER"];
    $_page['page'][0]['prefix'] ='form_'. $var;
    $_page['page'][0]['img_bg'] = $background;
    $_page['page'][0]['parent'] = $_SESSION['PROJECT']['menu']['type'];
    $_page['page'][0]['lock'] = true;
    $_page['page'][0]['menutype'] = $_SESSION['PROJECT']['menu']['type'];
    $_page['page'][0]['title'] = 'Form Contact Us';
    $_page['page'][0]['query_value'] = '';
    $_page['page'][0]['for'] = 'page_builder';
    $_page['page'][0]['menu'] = $file_name;
    $_page['page'][0]['js'] = '$ionicConfig.backButton.text("");';
    $_page['page'][0]['content'] = '
		<div class="list"  >
			<form ng-submit="submitContactUs()">

			<!-- input name -->
			<label class="item item-input item-stacked-label">
				<span class="input-label">Your Name</span>
				<input type="text" ng-model="form_contact_us.name" name="name" placeholder="Regel" ng-required="true"/>
			</label>
			<!-- ./input name -->

			<!-- input email -->
			<label class="item item-input item-stacked-label">
				<span class="input-label">Your Email</span>
				<input type="email" ng-model="form_contact_us.email" name="email" placeholder="name@domain.com" ng-required="true"/>
			</label>
			<!-- ./input email -->

			<!-- input subject -->
			<label class="item item-input item-stacked-label">
				<span class="input-label">Subject</span>
				<input type="text" ng-model="form_contact_us.subject" name="subject" placeholder="topic" ng-required="true"/>
			</label>
			<!-- ./input subject -->

			<!-- input type -->
			<label class="item item-input item-select noborder" ng-init="form_contact_us.type=\'Suggestion\'">
				<span class="input-label">Type</span>
				<select ng-model="form_contact_us.type" name="type" >
					<option value="Bug">Bug</option>
					<option value="Suggestion">Suggestion</option>
				</select>
			</label>
			<!-- ./input type -->

			<!-- input message -->
			<label class="item item-input item-stacked-label">
				<span class="input-label">Message</span>
				<textarea ng-model="form_contact_us.message" name="message" placeholder="Message" ng-required="true"></textarea>
			</label>
			<!-- ./input message -->

			<!-- input submit -->
			<div class="item item-button noborder">
				<button class="button button-assertive ink">Send Message</button>
			</div>
			<!-- ./input submit -->
			</form>
		</div>

		<br/><br/><br/><br/>
    ';
    file_put_contents(JSM_PATH . '/projects/' . $file_name . '/page.form_' . $var . '.json', json_encode($_page));


    buildIonic($file_name);
    header('Location: ./?page=x-page-builder&prefix=page_contact_us&target=' . $var);
    die();
}

$var = str2var($_GET['target']);
$pagebuilder_file = 'projects/' . $_SESSION['FILE_NAME'] . '/page_builder.contact_us.' . $var . '.json';

// TODO: page target
$project = new ImaProject();
$out_path = 'output/' . $file_name;
$preview_url = $out_path . '/www/#/' . $_SESSION['PROJECT']['app']['prefix'] . '/' . $var;

$raw_data = array();
if (file_exists($pagebuilder_file))
{
    $_raw_data = json_decode(file_get_contents($pagebuilder_file), true);
    $raw_data = $_raw_data['page_builder']['contact_us'][$var];
}

$option_page[] = array('label' => '< select page >', 'value' => '');
$z = 1;
foreach ($project->get_pages() as $page)
{
    $option_page[$z] = array('label' => 'Page `' . $page['prefix'] . '`', 'value' => $page['prefix']);
    if ($_GET['target'] == $page['prefix'])
    {
        $option_page[$z]['active'] = true;
    }
    $z++;
}

$form_input .= '<blockquote class="blockquote blockquote-warning">';
$form_input .= '<p>Your WordPress Plugin requires REST API v2 and plugin from WordPress Plugin Generator, then must be active in your WordPress Site.</p>';
$form_input .= '<ol>';
//$form_input .= '<li>Download <a href="https://wordpress.org/plugins/rest-api/">WordPress REST API 2</a> and <a href="https://wordpress.org/plugins/rest-api-helper/">REST API Helper</a></li>';
//$form_input .= '<li>Unzip and Upload `rest-api.xxx.zip` to the `/wp-content/plugins/rest-api` directory</li>';
//$form_input .= '<li>Activate the plugin through the \'plugins\' menu in WordPress</li>';
//$form_input .= '<li>Followed by unzip and Upload `rest-api-helper.xxx.zip` to the `/wp-content/plugins/rest-api-helper` directory</li>';
///$form_input .= '<li>Then activate the plugin through the \'plugins\' menu </li>';
//$form_input .= '<li>Now save and please fill in the fields below:</li>';
$form_input .= '</ol>';
$form_input .= '</blockquote>';
$form_input .= '<hr/>';
$form_input .= $bs->FormGroup('page_target', 'horizontal', 'select', 'Page Target', $option_page, 'Page will be overwritten', null, '4');
if ($_GET['target'] !== '')
{
    $form_input .= '<h4>Settings</h4>';
    $form_input .= $bs->FormGroup('contact_us[wp_url]', 'horizontal', 'text', 'WordPress URL', 'http://demo.ihsana.net/wordpress/', '', null, '7', $raw_data['wp_url']);
    $form_input .= $bs->FormGroup('contact_us[notes]', 'horizontal', 'textarea', 'Notes', '21', '', '', '7', $raw_data['notes']);
}

$footer .= '
<script src="./templates/default/vendor/tinymce/tinymce.min.js"></script>
<script type="text/javascript">

    tinymce.init({
        selector : "#page_banner_for_sell_notes_",
        force_br_newlines : false,
        force_p_newlines : false,
        forced_root_block : "",
        
    });

     $("#page_target").on("change",function(){
        window.location= "./?page=x-page-builder&prefix=page_contact_us&target=" +  $("#page_target").val() ;
        return false;
     });
</script>
';

?>