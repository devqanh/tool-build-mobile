<?php

/**
 * @author Jasman <jasman@ihsana.com>
 * @copyright Ihsana IT Solutiom 2016
 * @license Commercial License
 * 
 * @package Ionic App Builder
 */
if (isset($_SESSION['FILE_NAME']))
{
    $file_name = $_SESSION['FILE_NAME'];
} else
{
    header('Location: ./?page=dashboard&err=project');
    die();
}

if (isset($_POST['page-builder']))
{
    $json_save['page_builder']['full_webview']['site_url'] = htmlentities($_POST['full_webview']['site_url']);
    $json_save['page_builder']['full_webview']['type'] = htmlentities($_POST['full_webview']['type']);

    $site_url = $json_save['page_builder']['full_webview']['site_url'];
    $type_webview = $json_save['page_builder']['full_webview']['type'];

    file_put_contents('projects/' . $_SESSION['FILE_NAME'] . '/page_builder.full_webview.json', json_encode($json_save));


    $iframe_html = null;
    switch ($type_webview)
    {
        case 'iframe':
            $iframe_html = '
                            <ion-pane>
                              <ion-content scroll="true" overflow-scroll="true">
                        		<iframe ng-src="{{ \'' . $site_url . '\' | trustUrl }}"  class="fullscreen"></iframe>
                           	  </ion-content>
                            </ion-pane>
                        ';
            $js_json['js']['directives'] = '';
            $js_json['js']['router'] = '';
            file_put_contents('projects/' . $_SESSION['FILE_NAME'] . '/js.json', json_encode($js_json));
            break;
        case 'inappbrowser':
            $js_json['js']['directives'] = '
.run(function($ionicPlatform, $ionicLoading){
	$ionicPlatform.ready(function() {
		var ref = window.open("' . $site_url . '", "_blank","location=no");

        ref.addEventListener("loadstart", function() {
			ref.insertCSS({
				code: "body{background:#000;color:#fff;font-size:72px;}body:after{content:\'loading...\';position:absolute;bottom:50%;left:0;right:0; text-align: center; vertical-align: middle;}"
			});
		});

		ref.addEventListener("loadstop", function() {
			ref.insertCSS({
				code: ""
			});
		});

		ref.addEventListener("loaderror", function(){
            ref.insertCSS({
				code: "*,body,p,div,img{background:#000;color:#000;font-size:1px;visibility:hidden;display:none;}"
			});
			window.location = "retry.html";
		});


		ref.addEventListener("exit", function() {
			ionic.Platform.exitApp();
		});

	});
})
            ';
            $js_json['js']['router'] = '';
            file_put_contents('projects/' . $_SESSION['FILE_NAME'] . '/js.json', json_encode($js_json));
            break;
    }


    $html_code = '<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
    <title></title>
    <link href="lib/ionic/css/ionic.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
  </head>
  
  <body ng-app="' . $file_name . '" class="platform-android platform-cordova platform-webview" ng-controller="indexCtrl" id="{{ page_id }}">
    
        ' . $iframe_html . '
        
		<script src="lib/ionic/js/angular-chart/Chart.min.js"></script>
		<script src="lib/pdf/pdf.js"></script>
		<script src="lib/localforage/localforage.min.js"></script>
		<script src="lib/ionic/js/ionic.bundle.min.js"></script>
		<script src="lib/ionic/js/angular-utf8-base64/angular-utf8-base64.js"></script>
		<script src="lib/ionic/js/angular-chart/angular-chart.min.js"></script>
		<script src="lib/ionic/js/angular-md5/angular-md5.min.js"></script>
		<script src="lib/ionic-material/ionic.material.min.js"></script>
		<script src="lib/ion-md-input/js/ion-md-input.min.js"></script>
		<script src="lib/ionic-rating/ionic-rating.min.js"></script>
		<script src="lib/ion-slide-tabs/js/slidingTabsDirective.js"></script>
		<script src="lib/ion-datetime-picker/ion-datetime-picker.min.js"></script>
		<script src="lib/ionic-image-lazy-load/ionic-image-lazy-load.js"></script>
		<script src="lib/ionic/js/angular-cordova/ng-cordova.min.js"></script>
		<script src="cordova.js"></script>
		<script src="js/app.js"></script>
		<script src="js/controllers.js"></script>
		<script src="js/services.js"></script>
        
  </body>
  
</html>';

    buildIonic($file_name);
    file_put_contents('output/' . $file_name . '/www/index.html', $html_code);

}

$pagebuilder_file = 'projects/' . $_SESSION['FILE_NAME'] . '/page_builder.full_webview.json';
$raw_data = array();
if (file_exists($pagebuilder_file))
{
    $_raw_data = json_decode(file_get_contents($pagebuilder_file), true);
    $raw_data = $_raw_data['page_builder']['full_webview'];
}

if (!isset($raw_data['site_url']))
{
    $raw_data['site_url'] = 'http://your_site.com/';
    $raw_data['type'] = 'iframe';
}

$out_path = 'output/' . $file_name;
$preview_url = $out_path . '/www/index.html';

$_option_webviews[] = array('label' => 'iframe', 'value' => 'iframe');
$_option_webviews[] = array('label' => 'Cordova - inAppBrowser (recommended)', 'value' => 'inappbrowser');
$z = 0;
foreach ($_option_webviews as $_option_webview)
{
    $option_webview[$z] = $_option_webview;
    if ($_option_webview['value'] == $raw_data['type'])
    {
        $option_webview[$z]['active'] = true;
    }
    $z++;
}
$form_input .= '<blockquote class="blockquote blockquote-warning">';
$form_input .= '<ol>';

$form_input .= '<li>';
$form_input .= 'Webview for online web, add this code to .htaccess:';
$form_input .= '<code>Header set X-Frame-Options "ALLOWALL"</code>';
$form_input .= '</li>';

$form_input .= '<li>';
$form_input .= 'Webview type <code>InAppBrowser</code> only work in real device/android, ';
$form_input .= 'IMAB Emulator will be open new window so please ignore it';
$form_input .= '</li>';

$form_input .= '<li>';
$form_input .= '<p>Offline, create <code>new folder</code> on folder: <br/><code>' . realpath(JSM_PATH . '/output/' . $file_name . '/www/') . '</code></p>';
$form_input .= '</li>';

$form_input .= '<li>';
$form_input .= '<p>Not support for Admob Pro</p>';
$form_input .= '</li>';

$form_input .= '</blockquote>';
$form_input .= '<hr/>';
$form_input .= '<h4>Settings</h4>';
$form_input .= $bs->FormGroup('full_webview[site_url]', 'horizontal', 'text', 'Site URL', 'http://demo.ihsana.net/', '', null, '7', $raw_data['site_url']);
$form_input .= $bs->FormGroup('full_webview[type]', 'horizontal', 'select', 'Type', $option_webview, '', '', '4');

?>