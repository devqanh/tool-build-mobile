<?php

/**
 * @author Jasman <jasman@ihsana.com>
 * @copyright Ihsana IT Solutiom 2016
 * @license Commercial License
 * 
 * @package Ionic App Builder
 */

if (!defined('JSM_EXEC'))
{
    die(':)');
}

$file_name = 'test';
$bs = new jsmBootstrap();
$form_input = $html = $footer = null;


if (isset($_SESSION['FILE_NAME']))
{
    $file_name = $_SESSION['FILE_NAME'];
} else
{
    header('Location: ./?page=dashboard&err=project');
    die();
}

if (!isset($_SESSION["PROJECT"]['menu']))
{
    header('Location: ./?page=menu&err=new');
    die();
}

$out_path = 'output/' . $file_name;
$preview_url = $out_path . '/www/#/' . $_SESSION['PROJECT']['app']['prefix'] . '/';
$available_for_submit = true;
$require_target_page = false;
if (!isset($_GET['prefix']))
{
    $_GET['prefix'] = '';
}
if (!isset($_GET['source']))
{
    $_GET['source'] = '';
}

if (!isset($_GET['target']))
{
    $_GET['target'] = '';
}

if (isset($_GET['disable']))
{
    $pagebuilder_file = basename($_GET['disable']);
    rename(JSM_PATH . "/system/includes/page-builder/" . $pagebuilder_file . ".templates.php", JSM_PATH . "/system/includes/page-builder/" . $pagebuilder_file . ".templates.php.disable");
    header('Location: ?page=x-page-builder');
    die();
}
if (isset($_GET['enable']))
{
    $pagebuilder_file = basename($_GET['enable']);
    rename(JSM_PATH . "/system/includes/page-builder/" . $pagebuilder_file . ".templates.php.disable", JSM_PATH . "/system/includes/page-builder/" . $pagebuilder_file . ".templates.php");
    header('Location: ?page=x-page-builder');
    die();
}
$error = null;

$module_path = 'system/includes/page-builder/';
if (isset($_FILES['new_pagebuilder']))
{
    $tmp_name = $_FILES["new_pagebuilder"]["tmp_name"];
    $zip = new ZipArchive;
    if ($zip->open($tmp_name) === true)
    {
        $zip->extractTo($module_path);
        $zip->close();
        $error = '<div class="alert alert-success"><p>IMA Builder modules has been successfully installed</p></div>';
    } else
    {
        $error = '<div class="alert alert-danger"><p>This is not <strong>IMA Builder</strong> modules, please try again</p></div>';
    }
}


$content = null;
$content .= '<h4><span class="fa-stack fa-lg"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-book fa-stack-1x"></i></span>Extra Menus -&raquo; (IMAB) Page Builder</h4>';
if ($_GET['prefix'] != '')
{
    $content .= '<ul class="nav nav-tabs"><li><a href="./?page=x-page-builder">Modules</a></li><li class="active"><a href="#">Settings</a></li></ul>';
} else
{
    $content .= '<ul class="nav nav-tabs"><li class="active"><a href="#home" data-toggle="tab">Modules</a></li><li><a href="#new" data-toggle="tab">Module Manager</a></li></ul>';
}

$content .= '<br/>';

$content .= notice();
if ($_GET['prefix'] == '')
{
    $content .= '<div class="tab-content">';
    $content .= '<div class="tab-pane active" id="home">';
    $content .= '<h4>Free Templates from Other Users</h4>';
    $content .= '<blockquote class="blockquote blockquote-warning"><h4>Warning...!!!</h4><p>This template is derived <ins>contributions from other users</ins>, 
    IMA Builder does not test these templates as a whole, so no support for that.
    And you can also submit your template if you want to use by another user.</p></blockquote>';

    $content .= '
    <blockquote class="blockquote blockquote-danger">
    <h4>The rules that apply are:</h4>
    <ol>
    <li>Module with the prefix <code>page_*</code> only an effect for a <code>specific page</code>,</li>
    <li>and while beginning <code>eazy_*/setup_*</code> will affect <code>all the settings</code> either menu, table or page.</li>
    <li>Maybe, not suitable for <code>Smartcode Builder</code> features</li>
    </ol>
    </blockquote>';

    $content .= '<div class="row">';
    foreach (glob(JSM_PATH . "/system/includes/page-builder/*.templates.php") as $filename)
    {
        $readme = '-';
        $link_prefix = str_replace('.templates.php', '', basename(realpath($filename)));
        if (file_exists(JSM_PATH . "/system/includes/page-builder/" . $link_prefix . "/readme.txt"))
        {
            $readme = file_get_contents(JSM_PATH . "/system/includes/page-builder/" . $link_prefix . "/readme.txt");
        }
        $content .= '
    <div class="col-md-3">
    
        <div class="thumbnail" style="min-height: 430px;">
            <img src="./system/includes/page-builder/' . $link_prefix . '/assets/thumbnail.png" alt="' . $link_prefix . '" />
            <div class="caption">
            <h3 style="font-size:16px;font-weight: 600;margin:0;">' . ucwords(str_replace('_', ' ', $link_prefix)) . '</h3>
                <p style=" border-left-color: #ddd;border-left-style: solid;border-left-width: 3px;font-size:10px;margin-top: 6px;padding-left: 6px;">' . stripcslashes($readme) . '</p>
                <p style="position: absolute;bottom: 32px;"><a href="./?page=x-page-builder&prefix=' . $link_prefix . '" class="btn btn-primary" >Choose</a></p>
            </div>
        </div>
        
    </div>';
    }
    $content .= '</div>';
    $content .= '</div>';
    $content .= '<div class="tab-pane" id="new">';
    // TODO:

    $_content = null;
    $_content .= $bs->FormGroup('new_pagebuilder', 'default', 'file', 'Zip File', '', 'file ext *.zip', '', '8', '');
    $_content .= $bs->FormGroup(null, 'default', 'html', null, $bs->ButtonGroups(null, array(array(
            'name' => 'upload',
            'label' => 'Upload&raquo;',
            'tag' => 'submit',
            'color' => 'primary'))));

    // TODO: ---- | ---- MODULE MANAGER
    $content .= '<div class="panel panel-default">';
    $content .= '<div class="panel-heading"><h4 class="panel-title">New Modules</h4></div>';
    $content .= '<div class="panel-body">';
    $content .= $bs->Forms('module-setup', '', 'post', 'default', $_content);
    $content .= '</div>';
    $content .= '</div>';

    // TODO: ---- | ---- ENABLE MODULES
    $content .= '<div class="panel panel-default">';
    $content .= '<div class="panel-heading"><h4 class="panel-title">Enable Modules</h4></div>';
    $content .= '<div class="panel-body">';

    $content .= '<div class="table-responsive">';
    $content .= '<table class="table table-striped">';

    foreach (glob(JSM_PATH . "/system/includes/page-builder/*.templates.php") as $filename)
    {
        $link_prefix = str_replace('.templates.php', '', basename(realpath($filename)));
        $content .= '<tr>';
        $content .= '<td>' . $link_prefix . '</td>';
        $content .= '<td><a href="./?page=x-page-builder&disable=' . $link_prefix . '" class="btn btn-danger btn-sm">Disable</a></td>';
        $content .= '</tr>';
    }
    foreach (glob(JSM_PATH . "/system/includes/page-builder/*.templates.php.disable") as $filename)
    {
        $link_prefix = str_replace('.templates.php.disable', '', basename(realpath($filename)));
        $content .= '<tr>';
        $content .= '<td>' . $link_prefix . '</td>';
        $content .= '<td><a href="./?page=x-page-builder&enable=' . $link_prefix . '" class="btn btn-success btn-sm">Enable</a></td>';
        $content .= '</tr>';
    }
    $content .= '</table>';

    $content .= '</div>';
    $content .= '</div>';
    $content .= '</div>';


    $content .= '</div>';
    $content .= '</div>';
} else
{
    $how_to_use = null;

    $template_prefix = str2var($_GET["prefix"]);
    if (file_exists(JSM_PATH . "/system/includes/page-builder/" . $template_prefix . ".templates.php"))
    {
        require_once (JSM_PATH . "/system/includes/page-builder/" . $template_prefix . ".templates.php");
    }
    $content .= '<h4 style="border-bottom:1px solid #ddd;padding-bottom: 6px;">' . ucwords(str_replace('_', ' ', $template_prefix)) . '</h4>';

    $available_for_submit = true;
    if ($require_target_page == true)
    {
        if ($_GET['target'] == '')
        {
            $available_for_submit = false;
        }
    }

    if ($available_for_submit == true)
    {
        $form_input .= $bs->FormGroup(null, 'horizontal', 'html', null, $bs->ButtonGroups(null, array(array(
                'name' => 'page-builder',
                'label' => 'Save',
                'tag' => 'submit',
                'color' => 'primary'), array(
                'label' => 'Reset',
                'tag' => 'reset',
                'color' => 'default'))));
    }

    $content .= $how_to_use;
    $content .= $bs->Forms('page-builder-setup', '', 'post', 'horizontal', $form_input);


}


/**
 * create_page()
 * 
 * @param mixed $new_page_class
 * @param mixed $new_page_title
 * @param mixed $new_page_prefix
 * @param mixed $new_page_content
 * @param string $new_page_css
 * @param string $new_page_js
 * @param bool $background
 * @param string $icon
 * @param bool $scroll
 * @param bool $overflow_scroll
 * @param bool $add_to_menu
 * @param bool $title_tranparant
 * @param bool $remove_has_header
 * @param bool $remove_button_up
 * @param bool $remove_header
 * @return void
 */
function create_page($new_page_class, $new_page_title, $new_page_prefix, $new_page_content, $new_page_css = '', $new_page_js = '', $background = true, $icon = 'ion-android-star', $scroll = false, $overflow_scroll = false, $add_to_menu = true, $title_tranparant = false, $remove_has_header = false, $remove_button_up = false, $remove_header = false)
{

    if ($add_to_menu == true)
    {
        if (file_exists('projects/' . $_SESSION['FILE_NAME'] . '/menu.json'))
        {
            $raw_menu = json_decode(file_get_contents('projects/' . $_SESSION['FILE_NAME'] . '/menu.json'), true);
            $raw_menu['menu']['items'][] = array(
                "label" => $new_page_title,
                "var" => $new_page_prefix,
                "type" => "link",
                "icon-alt" => $icon,
                "icon" => $icon);
            $row_menu = array();
            $row_menus = array();

            foreach ($raw_menu['menu']['items'] as $row_menu)
            {
                $row_menus[$row_menu['var']] = $row_menu;
            }
            $row_menu = array();
            $raw_menu['menu']['items'] = array();
            foreach ($row_menus as $row_menu)
            {
                $raw_menu['menu']['items'][] = $row_menu;
            }


            file_put_contents('projects/' . $_SESSION['FILE_NAME'] . '/menu.json', json_encode($raw_menu));
        }
    }
    if ($background == true)
    {
        $img_link = 'data/images/background/bg9.jpg';
    } else
    {
        $img_link = '';
    }

    if (strlen($background) > 5)
    {
        $img_link = $background;
    }
    $new_page['page'][0] = array(
        'title' => $new_page_title,
        'prefix' => $new_page_prefix,
        'for' => '-',
        'last_edit_by' => 'page_builder',
        'parent' => $_SESSION['PROJECT']['menu']['type'],
        'menutype' => $_SESSION['PROJECT']['menu']['type'],
        'builder_link' => @$_SERVER["HTTP_REFERER"],
        'menu' => '-',
        'lock' => true,
        'class' => $new_page_class,
        'button_up' => 'bottom-right',
        'scroll' => true,
        'css' => $new_page_css,
        'js' => $new_page_js,
        'content' => $new_page_content,
        'img_bg' => $img_link);

    if (isset($_GET['target']))
    {
        foreach ($_SESSION['PROJECT']['page'] as $page)
        {
            if ($page['prefix'] == $_GET['target'])
            {
                if (!isset($page['button_up']))
                {
                    $page['button_up'] = 'none';
                }
                $new_page['page'][0]['for'] = $page['for'];
                $new_page['page'][0]['version'] = 'Upd.' . date('ymdhi');
                $new_page['page'][0]['parent'] = $page['parent'];
                $new_page['page'][0]['menutype'] = $page['menutype'];
                $new_page['page'][0]['button_up'] = $page['button_up'];
                $new_page['page'][0]['scroll'] = $scroll;
                $new_page['page'][0]['overflow-scroll'] = $overflow_scroll;

                if (isset($page['query']))
                {
                    $new_page['page'][0]['query'] = $page['query'];
                } else
                {
                    unset($new_page['page'][0]['query']);
                }

                if (isset($page['db_url_dinamic']))
                {
                    if ($page['db_url_dinamic'] == false)
                    {
                        $new_page['page'][0]['db_url_dinamic'] = false;
                    } else
                    {
                        $new_page['page'][0]['db_url_dinamic'] = 'on';
                    }
                } else
                {
                    $new_page['page'][0]['db_url_dinamic'] = false;
                }


            }
        }
    }

    if ($title_tranparant == true)
    {
        $new_page['page'][0]['title-tranparant'] = true;
    }
    if ($remove_has_header == true)
    {
        $new_page['page'][0]['remove-has-header'] = true;
    }

    if ($remove_button_up == true)
    {
        $new_page['page'][0]['button_up'] = 'none';
    }
    if ($remove_header == true)
    {
        $new_page['page'][0]['hide-navbar'] = true;
    }

    file_put_contents('projects/' . $_SESSION['FILE_NAME'] . '/page.' . $new_page_prefix . '.json', json_encode($new_page));
    buildIonic($_SESSION['FILE_NAME']);
    //header('Location: ./?page=page&prefix=' . $new_page_prefix);
    //exit(0);
}

class ImaProject
{

    function __construct()
    {

    }
    /**
     * ImaProject::get_tables()
     * 
     * @return
     */
    function get_tables()
    {
        $new_table = array();
        foreach ($_SESSION['PROJECT']['tables'] as $table)
        {
            $new_table[] = array('prefix' => $table['prefix'], "title" => $table['title']);
        }
        return $new_table;
    }
    /**
     * ImaProject::get_columns()
     * 
     * @param string $table
     * @return
     */
    function get_columns($table = '')
    {
        $new_column = array();
        if (is_array($_SESSION['PROJECT']['tables'][$table]['cols']))
        {
            foreach ($_SESSION['PROJECT']['tables'][$table]['cols'] as $cols)
            {
                $new_column[] = array('label' => $cols['label'], "value" => str2var($cols['title'], false));
            }
        }
        return $new_column;
    }
    /**
     * ImaProject::get_pages()
     * 
     * @return
     */
    function get_pages()
    {
        $new_page = array();
        foreach ($_SESSION['PROJECT']['page'] as $page)
        {
            if (!isset($page['for']))
            {
                $page['for'] = '-';
            }
            if (!isset($page['title']))
            {
                $page['title'] = '';
            }
            $new_page[] = array(
                'prefix' => $page['prefix'],
                "title" => $page['title'],
                'for' => $page['for']);
        }
        return $new_page;
    }
}

$icon = new jsmIonicon();
$modal_dialog = $icon->display();
$content .= $bs->Modal('icon-dialog', 'Ionicon Tables', $modal_dialog, 'md', null, 'Close', null);


$_page[] = googleplay_link();
$_page[] = mailto_link();

foreach ($_SESSION['PROJECT']['page'] as $page)
{
    $param_query = null;
    if (isset($page['query']))
    {
        $param_query = '/1';
    }
    $_page[] = '#/' . $file_name . '/' . $page['prefix'] . $param_query;
}

$content .= '<script type="text/javascript">';
$content .= 'var typehead_vars = ' . json_encode($_page) . ';';
$content .= '</script>';

$template->demo_url = $preview_url;
$template->title = $template->base_title . ' | ' . 'Extra Menus -&raquo; Page Builder';
$template->base_desc = 'Page Builder';
$template->content = $content;
$template->emulator = true;
$template->footer = $footer;
